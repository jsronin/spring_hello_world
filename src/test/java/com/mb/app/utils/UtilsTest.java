package com.mb.app.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mb.app.utils.Utils.*;
import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

class UtilsTest {

    public static String[] greetingList;

    @BeforeEach
    void setUp() {
        greetingList = new String[]{
                null,
                "",
                "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
                "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901",
                "aAbB 12 ,.",
                "aAbB 12 ,. &*%$"};
    }

    @AfterEach
    void tearDown() {
        greetingList = null;
        System.gc();
    }

    @Test
    void isValidTest() {
        assertFalse(isValid(greetingList[0]));
        assertFalse(isValid(greetingList[1]));
        assertTrue(isValid(greetingList[2]));
        assertFalse(isValid(greetingList[3]));
        assertTrue(isValid(greetingList[4]));
        assertFalse(isValid(greetingList[5]));
    }

    @Test
    void isNotEmptyTest() {
        assertFalse(isNotEmpty(greetingList[0]));
        assertFalse(isNotEmpty(greetingList[1]));
        assertTrue(isNotEmpty(greetingList[2]));
    }

    @Test
    void hasValidLengthTest() {
        assertTrue(hasValidLength(greetingList[2]));
        assertFalse(hasValidLength(greetingList[3]));
    }

    @Test
    void hasValidContentTest() {
        assertTrue(hasValidContent(greetingList[4]));
        assertFalse(hasValidContent(greetingList[5]));
    }
}