package com.mb.app.services;

import com.mb.app.models.Hello;
import com.mb.app.repositories.HelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is a simple implementation of Hello service
 */
@Service
public class HelloServiceImpl implements HelloService {

    @Autowired
    HelloRepository helloRepository;

    /**
     * This method is for adding new Hello entity
     *
     * @param hello is Hello entity
     * @return
     */
    @Override
    public Hello addHello(Hello hello) {
        return helloRepository.save(hello);
    }

    /**
     * This method is for getting specific Hello entity by id
     *
     * @param id is id of Hello entity
     * @return
     */
    @Override
    public Hello getHelloById(Integer id) {
        return helloRepository.findById(id).orElse(null);
    }

    /**
     * This method returns a list of Hello entity
     *
     * @return
     */
    @Override
    public Iterable<Hello> getHelloList() {
        return helloRepository.findAll();
    }

    /**
     * This method is for getting Hello entity list by greeting
     *
     * @param greeting is greeting field of weather entity
     * @return
     */
    @Override
    public Iterable<Hello> getHelloListByGreeting(String greeting) {
        return helloRepository.findByGreeting(greeting);
    }

}
