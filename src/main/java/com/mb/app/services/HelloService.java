package com.mb.app.services;

import com.mb.app.models.Hello;

public interface HelloService {

    Hello addHello(Hello hello);

    Hello getHelloById(Integer id);

    Iterable<Hello> getHelloList();

    Iterable<Hello> getHelloListByGreeting(String greeting);

}
