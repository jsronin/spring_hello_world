package com.mb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * This is Hello entity , with sample fields
 */
@Entity
@Table(name = "hello", indexes = @Index(columnList = "greeting"))
public class Hello {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private LocalDateTime dateTime;

    private String greeting;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Hello))
            return false;
        Hello other = (Hello) o;
        return this.id.equals(other.id) &&
                this.dateTime.equals(other.dateTime) &&
                this.greeting.equals(other.greeting);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (id != null) {
            result = 31 * result + id.hashCode();
        }
        if (dateTime != null) {
            result = 31 * result + dateTime.hashCode();
        }
        if (greeting != null) {
            result = 31 * result + greeting.hashCode();
        }
        return result;
    }

}
