package com.mb.app.repositories;

import com.mb.app.models.Hello;
import org.springframework.data.repository.CrudRepository;

public interface HelloRepository extends CrudRepository<Hello, Integer> {

    Iterable<Hello> findByGreeting(String greeting);

}
