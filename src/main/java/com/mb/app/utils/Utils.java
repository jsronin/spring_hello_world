package com.mb.app.utils;

/**
 * This is a sample Util for validating greeting
 */
public class Utils {

    public static final int GREETING_MAX_LENGTH = 100;
    public static final String GREETING_PATTERN = "[A-Za-z0-9,. ]*";

    /**
     * This method checks validity of greeting
     *
     * @param greeting
     * @return
     */
    public static boolean isValid(String greeting) {
        boolean result = false;
        if (isNotEmpty(greeting) &&
                hasValidLength(greeting) &&
                hasValidContent(greeting)) {
            result = true;
        }
        return result;
    }

    /**
     * This method checks nullable
     *
     * @param greeting
     * @return
     */
    public static boolean isNotEmpty(String greeting) {
        boolean result = true;
        if (greeting == null || greeting.length() <= 0) {
            result = false;
        }
        return result;
    }

    /**
     * This method checks valid length
     *
     * @param greeting
     * @return
     */
    public static boolean hasValidLength(String greeting) {
        boolean result = false;
        if (isNotEmpty(greeting) && greeting.length() <= GREETING_MAX_LENGTH) {
            result = true;
        }
        return result;
    }

    /**
     * This methos checks content according to pattern
     *
     * @param greeting
     * @return
     */
    public static boolean hasValidContent(String greeting) {
        boolean result = false;
        if (isNotEmpty(greeting) && greeting.matches(GREETING_PATTERN)) {
            result = true;
        }
        return result;
    }
}
