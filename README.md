Spring Boot Hello World
---

Welcome to the Spring Boot Hello World!

### Introduction

This is a simple application that just shows a Hello World Page and can use MySql DB for storing its data

### The task
Design a project to display hello world.
a) Use any view page like jsp, html etc.
b) Use web framework as controller
c) Use MYSQL db to storing values

##How to build and run
* mvn clean install
* mvn spring-boot:run
###For Docker deployment
* docker build may_bank
* docker run -p 8000:8000 may_bank
###API Documentation
* http://localhost:8000/swagger-ui.html
###Default Host and Port
* localhost:8000
* localhost:8000/hello
